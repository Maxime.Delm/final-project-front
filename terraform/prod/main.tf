terraform {
  required_providers {

    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.51.0"
    }

    ovh = {
      source  = "ovh/ovh"
    }
  }

  backend "s3" {
    bucket                  = "tf-frontend-morningnews-prod"
    key                     = "state/terraform.tfstate"
    region                  = "eu-north-1"
  }
}


provider "aws" {
  region  = "eu-north-1"
}

provider "ovh" {
  endpoint            = "ovh-eu"
  application_key     = var.ovh_application_key
  application_secret  = var.ovh_application_secret
  consumer_key        = var.ovh_consumer_key
}

# Fetch the Debian AMI - debian-12-amd64-20240507-1740 for the region used
data "aws_ami" "debian_12" {
  most_recent = true
  owners      = ["136693071363"]

  filter {
    name   = "name"
    values = ["debian-12-amd64-20240507-1740"]
  }
}

# Remote state to access backend outputs
data "terraform_remote_state" "common" {
  backend = "s3"
  config = {
    bucket = "tf-backend-morningnews-prod"
    key    = "state/terraform.tfstate"
    region = "eu-north-1"
  }
}

# ===================================
# VPC - We will use the vpc created in the backend and retrieved with 
# the terraform state
# ===================================

## -- vpc for tests

# Create the "jenkins_vpc"
resource "aws_vpc" "morningnews_vpc" {
  cidr_block = "192.168.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true
  tags = {
    Name = "morningnews-vpc"
  }
}

# ===================================
# Security
# ===================================


# ---- Security Group -- >

# Create group "morningnews_front_prod_sg"
resource "aws_security_group" "morningnews_front_sg" {
  name        = var.sg
  description = "Security group for morningnews"
  vpc_id      = data.terraform_remote_state.common.outputs.vpc_id

  tags = {
    Name = "sg-${var.sg}--${var.environment}"
  }
}

# Add Inbound ssh rule to group morningnews_front_prod_sg
resource "aws_vpc_security_group_ingress_rule" "allow_ssh" {
  count             = length(var.ssh_access_cidr)
  security_group_id = aws_security_group.morningnews_front_sg.id
  cidr_ipv4         = var.ssh_access_cidr[count.index]
  ip_protocol       = "tcp"
  from_port         = 22
  to_port           = 22
}


# Add Inbound http rule to group morningnews_front_prod_sg
resource "aws_vpc_security_group_ingress_rule" "allow_inbound_custom" {
  count             = length(var.http_access_cidr)
  security_group_id = aws_security_group.morningnews_front_sg.id
  cidr_ipv4         = var.http_access_cidr[count.index]
  ip_protocol       = "tcp"
  from_port         = 3001
  to_port           = 3001
}

# Add Inbound http rule to group morningnews_front_prod_sg
resource "aws_vpc_security_group_ingress_rule" "allow_inbound_http" {
  count             = length(var.http_access_cidr)
  security_group_id = aws_security_group.morningnews_front_sg.id
  cidr_ipv4         = var.http_access_cidr[count.index]
  ip_protocol       = "tcp"
  from_port         = 80
  to_port           = 80
}

# Add Inbound https rule to group morningnews_front_prod_sg
resource "aws_vpc_security_group_ingress_rule" "allow_inbound_https" {
  count             = length(var.https_access_cidr)
  security_group_id = aws_security_group.morningnews_front_sg.id
  cidr_ipv4         = var.https_access_cidr[count.index]
  ip_protocol       = "tcp"
  from_port         = 443
  to_port           = 443
}

# Add Outbound rules to "jenkins_sg"
resource "aws_vpc_security_group_egress_rule" "allow_all_outbound" {
  count             = length(var.ssh_access_cidr)
  security_group_id = aws_security_group.morningnews_front_sg.id
  cidr_ipv4         = var.ssh_access_cidr[count.index]
  ip_protocol       = "-1" # ✿ semantically equivalent to all ports
}

# ---- Keys -- >

# Generate an RSA key type
resource "tls_private_key" "rsa_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

# Key pair
# Create an AWS Key Pair using the generated public key
resource "aws_key_pair" "morningnews_front_key" {
  key_name   = "morningnews_front_key_pair"
  public_key = tls_private_key.rsa_key.public_key_openssh
}

# Private key
# Create a secret manager resource to store private key
resource "aws_secretsmanager_secret" "private_key_secret" {
  name = "morningnews_front_private_key"
  recovery_window_in_days = 0 # imediate deletion

  lifecycle {
    ##prevent_destroy = true
    #create_before_destroy = true  # Ensure to destroy before creating a new key
  }
}

# Private key
# Stores the key in in AWS Secrets Manager so it is secure
resource "aws_secretsmanager_secret_version" "private_key" {
  secret_id     = aws_secretsmanager_secret.private_key_secret.id
  secret_string = tls_private_key.rsa_key.private_key_pem
}

# Public key
# Stores the key in S3 for accessibility for both backend and frontend
resource "aws_s3_object" "public_key" {
  bucket               = "tf-backend-morningnews-${var.environment}"
  key                  = "key/project_key.pub"
  content              = tls_private_key.rsa_key.public_key_openssh
  acl                  = "private"
  server_side_encryption = "AES256"
}

# ===================================
# EC2
# ===================================

# Instance for our frontend
resource "aws_instance" "morningnews_vm" {
  ami           = "ami-0506d6d51f1916a96"
  instance_type = "t3.medium"
  key_name      = aws_key_pair.morningnews_front_key.key_name
  associate_public_ip_address = true
  subnet_id              = data.terraform_remote_state.common.outputs.public_subnet_id #aws_subnet.public_subnet_prod.id
  vpc_security_group_ids = [aws_security_group.morningnews_front_sg.id]

  tags = {
    Name = "frontend_instance_${var.environment}"
  }
}


# ===================================
# DNS
# ===================================

resource "ovh_domain_zone_record" "sub_morningnews" {
  zone       = var.domain_name
  fieldtype  = "A"
  subdomain  = "front"
  ttl        = 3600
  target     = aws_instance.morningnews_vm.public_ip
}

# ===================================
# Ansible variables
# ===================================

# Generate Ansible variables file in the group_vars folder
resource "local_file" "morningnews_vars" {
  filename = "../../ansible/${var.environment}/group_vars/morningnews.yml"
  content = <<-EOF
    ansible_user: admin
    ansible_ssh_private_key_file:  ./${var.environment}/${aws_key_pair.morningnews_front_key.key_name}.pem
    ansible_port: 22
    public_ip: "${aws_instance.morningnews_vm.public_ip}"
    instance_dns_zone : ${ovh_domain_zone_record.sub_morningnews.zone}
    instance_dns_subdomain: ${ovh_domain_zone_record.sub_morningnews.subdomain}
    certbot_email: diwap91874@crodity.com
    hostname: morningnews
  EOF
}

# Generate Ansible hosts file
resource "local_file" "hosts" {
  content = <<-EOF
  [morningnews]
  ${aws_instance.morningnews_vm.public_ip}

  [morningnews-back]
  ${data.terraform_remote_state.common.outputs.backend_prod_ip}
  EOF
  filename = "../../ansible/${var.environment}/hosts"
}